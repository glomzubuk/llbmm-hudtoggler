
Just a little mod I developped for Jawdrop that allows toggling the in-game HUD and display cinematic lines on the screen.
Default key is "H", and can be configured in mod menu. 

Pressing the key after the ball spawns and before the "Play Ball" goes away will softlock your game.
Either use the config to disable the hud on start or wait after the "Play Ball" has left the screen.

No injection here, so this mod is safe to uninstall.
