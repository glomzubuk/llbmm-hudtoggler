﻿using System;
using System.Collections;
using UnityEngine;
using LLScreen;
using LLHandlers;

namespace HudToggler
{
    public class HudToggler : MonoBehaviour
    {
        #region modinfos
        public const string modVersion = "v0.1";
        #endregion

        private bool InMatch
        {
            get
            {
                JOFJHDJHJGI currentGameState = DNPFJHMAIBP.HHMOGKIMBNM();
                return currentGameState == JOFJHDJHJGI.CDOFDJMLGLO || currentGameState == JOFJHDJHJGI.LGILIJKMKOD;
            }
        }

        #region configs
        private KeyCode toggleHudKey = KeyCode.H;
        private bool enableHudOnStart = false;
        private bool enableLinesOnHudHidden = true;
        private int linesInAnimationDuration = 4000;
        private int linesOutAnimationDuration = 250;
        private int linesWitdh = 10;

        #endregion

        public static ModMenuIntegration MMI = null;
        public static HudToggler instance = null;

        public static void Initialize()
        {
            GameObject gameObject = new GameObject("HudToggler");
            instance = gameObject.AddComponent<HudToggler>();
            DontDestroyOnLoad(gameObject);
        }

        private ScreenGameHud sgh = null;
        private bool activelyHideHUD = true;
        private bool hudEnabled = true;
        void Update()
        {
            if (MMI == null) { MMI = gameObject.AddComponent<ModMenuIntegration>(); }
            else
            {
                //foreach (var pair in MMI.configKeys) Console.WriteLine(pair.Key + " | " + pair.Value);
                toggleHudKey = MMI.GetKeyCode(MMI.configKeys["(key)toggleHudKey"]);
                enableHudOnStart = MMI.GetTrueFalse("(bool)enableHudOnStart");
                enableLinesOnHudHidden = MMI.GetTrueFalse("(bool)enableLinesOnHudHidden");
                linesInAnimationDuration = MMI.GetInt("(int)linesInAnimationDuration");
                linesOutAnimationDuration = MMI.GetInt("(int)linesOutAnimationDuration");
                linesWitdh = MMI.GetInt("(int)linesWitdh");
            }
            if (InMatch)
            {
                if (sgh == null)
                {
                    sgh = FindObjectOfType<ScreenGameHud>();
                    hudEnabled = enableHudOnStart;
                    if (hudEnabled == false)
                    {
                        activelyHideHUD = true;
                        if (enableLinesOnHudHidden)
                            this.ShowScreenLines(true);
                    }

                }
                else
                {
                    if (!hudEnabled)
                    {
                        if (activelyHideHUD)
                            this.ForceHideHUD(sgh, true);
                    }

                    if (Input.GetKeyDown(toggleHudKey) &&
                        BallHandler.instance != null && !BallHandler.instance.NoBallsActivelyInMatch())
                    {
                        if (hudEnabled)
                        {
                            base.StartCoroutine(sgh.KShowHud(false));
                            hudEnabled = false;
                            if (enableLinesOnHudHidden)
                                this.ShowScreenLines(true);
                        }
                        else
                        {
                            if (activelyHideHUD)
                            {
                                activelyHideHUD = false;
                                this.ForceHideHUD(sgh, false);
                            }
                            if (enableLinesOnHudHidden)
                                this.ShowScreenLines(false);
                            base.StartCoroutine(sgh.KShowHud(true));
                            hudEnabled = true;
                        }
                    }
                }
            }
            else
            {
                sgh = null;
                playerInfos = null;
            }
        }

        void OnGUI()
        {
            if (InMatch)
            {
                DrawRect(topScreenLine.rect, Color.black);
                DrawRect(bottomScreenLine.rect, Color.black);
            }
        }

        GameHudPlayerInfo[] playerInfos = null;
        private void ForceHideHUD(ScreenGameHud _sgh, bool shouldShow)
        {
            int eulerAngleX = shouldShow ? 90 : 0;
            if (_sgh.rtBoomBox.hasChanged)
                _sgh.rtBoomBox.eulerAngles = new Vector3(eulerAngleX, 0, 0);
            if (playerInfos == null)
                playerInfos = _sgh.playerInfos;
            foreach (GameHudPlayerInfo playerInfo in playerInfos)
            {

                if (playerInfo != null && playerInfo.transform != null)
                    playerInfo.transform.eulerAngles = new Vector3(eulerAngleX, 0, 0);
            }
        }


        class RectContainer //TODO Really ugly, find a better way for the Coroutine to set the rect position, can't use refs
        {
            public Rect rect;
            public RectContainer(Rect rect)
            {
                this.rect = rect;
            }
        }
        private RectContainer topScreenLine = new RectContainer(new Rect(new Vector2(0, -Screen.height), new Vector2(Screen.width, Screen.height)));
        private RectContainer bottomScreenLine = new RectContainer(new Rect(new Vector2(0, Screen.height), new Vector2(Screen.width, Screen.height)));
        private Coroutine topLineCoroutine;
        private Coroutine bottomLineCoroutine;
        private void ShowScreenLines(bool shouldShow)
        {
            if (topLineCoroutine != null) this.StopCoroutine(topLineCoroutine);
            if (bottomLineCoroutine != null) this.StopCoroutine(bottomLineCoroutine);

            int lineWidth = (Screen.height / 100) * this.linesWitdh;
            float inAnimationDuration = (float)linesInAnimationDuration / 1000;
            float outAnimationDuration = (float)linesOutAnimationDuration / 1000;

            if (shouldShow)
            {
                topLineCoroutine = this.StartCoroutine(this.CAnimVert(inAnimationDuration, topScreenLine, -Screen.height, -Screen.height + lineWidth));
                bottomLineCoroutine = this.StartCoroutine(this.CAnimVert(inAnimationDuration, bottomScreenLine, Screen.height, Screen.height - lineWidth));
            }
            else
            {
                topLineCoroutine = this.StartCoroutine(this.CAnimVert(outAnimationDuration, topScreenLine, Screen.height - lineWidth, Screen.height));
                bottomLineCoroutine = this.StartCoroutine(this.CAnimVert(outAnimationDuration, bottomScreenLine, -Screen.height + lineWidth, -Screen.height));
            }
        }

        private IEnumerator CAnimVert(float dur, RectContainer rectC, float y1, float y2)
        {
            Vector2 pos = rectC.rect.position;
            pos.y = y1;
            Vector2 pos2 = new Vector2((float)(int)pos.x, (float)(int)y2);
            for (float f = 0f; f < 1f; f += Time.deltaTime / dur)
            {
                rectC.rect.position = Vector2.Lerp(pos, pos2, f);
                yield return null;
                if (rectC.rect == null)
                {
                    yield break;
                }
            }
            rectC.rect.position = pos2;
            yield break;
        }

        private static readonly Texture2D backgroundTexture = Texture2D.whiteTexture;
        private static readonly GUIStyle textureStyle = new GUIStyle { normal = new GUIStyleState { background = backgroundTexture } };
        public static void DrawRect(Rect position, Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

        public static void LayoutBox(Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUILayout.Box(content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }
    }
}
