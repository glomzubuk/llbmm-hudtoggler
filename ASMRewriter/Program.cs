﻿using System;
using System.IO;
using Mono.Cecil;
using Mono.Cecil.Inject;

namespace ASMRewriter
{
    class Program
    {
        private static readonly string ASMName = "Assembly-CSharp.dll";
        private static readonly string ModName = "HudToggler";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                var finalASMPath = args[0].Replace("%20", " ");
                Directory.SetCurrentDirectory(finalASMPath);
            }

            Console.WriteLine("Reading assembly...");
            // Here Path.Combine is required for ASMR to run under linux
            /*AssemblyDefinition modDef = AssemblyDefinition.ReadAssembly(
                Path.Combine(Directory.GetCurrentDirectory(), ModName + ".dll"),
                new ReaderParameters { ReadWrite = true }
            );*/
            try
            {
                // Same thing here
                using (AssemblyDefinition aDef = AssemblyDefinition.ReadAssembly(
                    Path.Combine(Directory.GetCurrentDirectory(), ASMName),
                    new ReaderParameters { ReadWrite = true })
                )
                {
                    try
                    {
                        asm.SetFieldPublic(aDef, "LLScreen.ScreenGameHud", "playerInfos");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception caught: ");
                        Console.WriteLine(e);
                    }
                    try { aDef.Write(); } catch (Exception ex) { Console.WriteLine(ex); }
                    aDef.Dispose();
                }
            }
            catch
            {
                Console.WriteLine("Could not open assembly, press Enter to exit...");
                Console.ReadLine();
            }

            //modDef.Dispose();
        }
    }

    public enum ChangeType
    {
        Public = 0,
        Virtual = 1,
        Abstract = 2
    }
}
